import os
from dotenv import load_dotenv

load_dotenv()

TOKEN = os.getenv("TOKEN", '')

USER = os.getenv("USER", '')
PASSWORD = os.getenv("PASSWORD", '')
HOST = os.getenv("HOST", "127.0.0.1")
PORT = os.getenv("PORT", "5432")
DB = os.getenv("DB", '')
DSN = f"postgresql://{USER}:{PASSWORD}@{HOST}:{PORT}/{DB}"
