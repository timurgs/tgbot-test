CREATE TABLE IF NOT EXISTS departament (
	id SERIAL PRIMARY KEY,
	name TEXT NOT NULL UNIQUE
);


CREATE TABLE IF NOT EXISTS users (
	id SERIAL PRIMARY KEY,
	name TEXT NOT NULL,
	tg TEXT NOT NULL UNIQUE,
	departament_id INTEGER NOT NULL REFERENCES departament(id)
);


CREATE TABLE IF NOT EXISTS project (
	id SERIAL PRIMARY KEY,
	name TEXT NOT NULL,
	url TEXT NOT NULL,
	status BOOLEAN NOT NULL
);


CREATE TABLE IF NOT EXISTS user_project (
	user_id INTEGER REFERENCES users(id),
	project_id INTEGER REFERENCES project(id),
	constraint pk primary key (user_id, project_id)
);