# Тестовое задание "Telegram-бот"

1. Для связи ТГ-бота с БД я использовал отдельный
класс в котором прописал подключение и реализовал
отдельные методы под каждое действие.
2. БД заполняется различными действиями в боте:
   1. При регистрации пользователя (обработчик ```name_input``` 
   и дальнейшая логика с состояниями) передаются данные.
   2. При выборе нового проекта.
3. Вывод иформации демонстрирует обработчик ```info_output```.
4. Кнопочная форма реализована:
   - также при регистрации, когда нужно выбирать 
   направление (inline-ссылки и callbacks) и проект;
   - при выборе нового проекта.
5. Реализованы команды: /start, /help, /reg,
/project, /my_info.

В школе необходимо улучшить и автоматизировать найм участников. Идея
состоит в том, что каждый должен будет зарегистрироваться в системе, указывая
свое имя, направление и проект, в котором он хочет 
участвовать и который на данный момент проводит набор.
Для того, чтобы не было "безработных" людей, проект
выбирается сразу.

Также можно выбрать еще один проект, если человек
зарегистрирован.