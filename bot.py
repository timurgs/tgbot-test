#!venv/bin/python
from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.utils.callback_data import CallbackData

from database import DataBase
from config import TOKEN


db = DataBase()

bot = Bot(token=TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())

cb = CallbackData("check", "names")


class Reg(StatesGroup):
    name = State()
    departament = State()
    project = State()


class Project(StatesGroup):
    proj = State()


@dp.message_handler(commands=['start'], state="*")
async def start(message: types.Message):
    await message.answer("Добро пожаловать в Школу IT! Для регистрации используйте "
                         "команду /reg")


@dp.message_handler(commands=['help'], state="*")
async def help(message: types.Message):
    await message.answer("Доступные команды:\n/start - Приветствие\n/reg - Регистрация\n"
                         "/project - Выбор проекта\n/my_info - Ваша информация")


@dp.message_handler(commands=['my_info'], state="*")
async def info_output(message: types.Message):
    user = db.get_user(message.from_user.username)
    if user:
        departaments = db.get_departaments()
        for departament in departaments:
            if departament[0] == user[0][2]:
                project_ids = db.get_user_project(user[0][0])
                projects = db.get_projects()
                projects_output = []
                for project_id in project_ids:
                    for project in projects:
                        if project_id[0] == project[0]:
                            projects_output.append(project[1])
                            break
                projects_output = ', '.join(projects_output)
                await message.answer(f"Name: {user[0][1]}\nDepartament: {departament[1]}\n"
                                     f"Projects: {projects_output}")
    else:
        await message.answer(f"Вы не зарегистрированы!")


async def keyboard_call_select_project():
    reply_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    inline_keyboard = types.InlineKeyboardMarkup(row_width=1)

    projects = db.get_projects()
    for project in projects:
        reply_keyboard.add(project[1])
        button = types.InlineKeyboardButton(text=project[1], url=project[2])
        inline_keyboard.add(button)
    return inline_keyboard, reply_keyboard


async def choose_wrong_project(message_param):
    projects = db.get_projects()
    project_names = {project[1] for project in projects}
    if message_param.text not in project_names:
        await message_param.answer("Пожалуйста, выберите проект, используя клавиатуру ниже.")
        return
    else:
        return projects


async def fill_intermediate_table(message_param, projects):
    user = db.get_user(message_param.from_user.username)

    for project in projects:
        if project[1] == message_param.text:
            project_ids = db.get_user_project(user[0][0])
            for project_id in project_ids:
                if project[0] == project_id[0]:
                    return
            user_project_ids = (user[0][0], project[0])
            db.add_user_project(user_project_ids)
            break
    return not None


@dp.message_handler(commands=['project'], state="*")
async def project_selection(message: types.Message):
    user = db.get_user(message.from_user.username)
    if user:
        keyboard = await keyboard_call_select_project()
        await Project.proj.set()
        await message.answer("Кнопки-ссылки", reply_markup=keyboard[0])
        await message.answer("Выберите из списка доступный проект", reply_markup=keyboard[1])
    else:
        await message.answer("Вы не зарегистрированы!")


@dp.message_handler(state=Project.proj)
async def check_select_project(message: types.Message, state: FSMContext):
    projects = await choose_wrong_project(message)
    if projects:
        fill = await fill_intermediate_table(message, projects)
        if fill:
            await message.answer(f'Проект: {message.text}\n'
                                 f'Откликайтесь. Мы будем вам рады!')
            await state.reset_state()
        else:
            await message.answer(f'Вы уже в проекте!')
            await state.reset_state()
    else:
        return


@dp.message_handler(commands=['reg'], state="*")
async def name_input(message: types.Message):
    user = db.get_user(message.from_user.username)
    if user:
        await message.answer("Вы уже зарегистрированы!")
    else:
        await message.reply("Введите свое имя")
        await Reg.name.set()


@dp.message_handler(state=Reg.name)
async def departament_selection(message: types.Message, state: FSMContext):
    await state.update_data(entered_name=message.text)

    keyboard = types.InlineKeyboardMarkup(row_width=1)

    departaments = db.get_departaments()
    for departament in departaments:
        button = types.InlineKeyboardButton(text=departament[1], callback_data=cb.new(names=departament[1]))
        keyboard.add(button)
    await Reg.next()
    await message.answer("Выберите направление:", reply_markup=keyboard)


# Здесь может быть логика выбора стэка, а далее подходящий проект под него


@dp.callback_query_handler(cb.filter(), state=Reg.departament)
async def project_selection(call: types.CallbackQuery, state: FSMContext):
    departaments = db.get_departaments()
    cb_departament = call.data.split(":")[1]
    for departament in departaments:
        if departament[1] == cb_departament:
            await state.update_data(id_selected_departament=departament[0])
    await state.update_data(selected_departament=cb_departament)

    keyboard = await keyboard_call_select_project()
    await Reg.next()
    await bot.send_message(call.from_user.id, "Кнопки-ссылки", reply_markup=keyboard[0])
    await bot.send_message(call.from_user.id, "Теперь выберите из списка доступный проект", reply_markup=keyboard[1])


@dp.message_handler(state=Reg.project)
async def result_output(message: types.Message, state: FSMContext):
    projects = await choose_wrong_project(message)
    if projects:
        user_data = await state.get_data()

        data = (user_data["entered_name"], message.from_user.username,
                user_data["id_selected_departament"])
        db.add_user(data)

        await fill_intermediate_table(message, projects)

        await message.answer(f'{user_data["entered_name"]}, '
                             f'{user_data["selected_departament"]}\n'
                             f'Проект "{message.text}"\n'
                             f'Откликайтесь. Мы вас ждем!')
        await state.reset_state()
    else:
        return


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)
