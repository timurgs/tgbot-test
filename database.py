import psycopg2
from config import DSN


class DataBase:
    def __init__(self):
        """Подключение к базе данных"""
        self.connection = psycopg2.connect(DSN)
        self.cursor = self.connection.cursor()

    def add_user(self, data):
        """Добавление пользователя в БД"""
        with self.connection:
            query = "INSERT INTO users(name, tg, departament_id) VALUES (%s, %s, %s)"
            self.cursor.executemany(query, [data])
            self.connection.commit()

    def get_user(self, tg):
        """Получение пользователя"""
        with self.connection:
            self.cursor.execute("SELECT id, name, departament_id FROM users WHERE tg = '%s'" % tg)
            self.connection.commit()
            return self.cursor.fetchall()

    def add_user_project(self, data):
        """Добавление данных в промежуточную таблицу"""
        with self.connection:
            query = "INSERT INTO user_project(user_id, project_id) VALUES (%s, %s)"
            self.cursor.executemany(query, [data])
            self.connection.commit()

    def get_departaments(self):
        """Получение списка отделов"""
        with self.connection:
            self.cursor.execute("SELECT * FROM departament")
            self.connection.commit()
            return self.cursor.fetchall()

    def get_projects(self):
        """Получение списка проектов"""
        with self.connection:
            self.cursor.execute("SELECT id, name, url FROM project WHERE status = True")
            self.connection.commit()
            return self.cursor.fetchall()

    def get_user_project(self, user_id):
        """Получение данных из промежуточной таблицы"""
        with self.connection:
            self.cursor.execute("SELECT project_id FROM user_project WHERE user_id = '%s'" % user_id)
            self.connection.commit()
            return self.cursor.fetchall()
